#include <iostream>
#include <string.h>
#include <string>

using namespace std;


class Stack{
	public:
		void Push(string Title);
		void Pop();
		void Display();
		void viewPlaylist();// in view menu
			int addMusic(int add, int count); //add to playlist
			int removeMusic(int count);  // remove from playlist
	private:
		struct Node // struct
		{
			Node* next;
			int data, Playlist; 
			string Title;
			bool checker;	
		} typedef *pointer; // set as pointer datatype
		pointer head;
		pointer current;
		pointer top;
		pointer temp;
};
void Stack::Push(string Title) //push stacks
{
	pointer link = new Node; // creating new node
	link->next = NULL; //no data
		if(head!=NULL) //empty head
			{
			current = head;
			temp = head;
			while(current->next != NULL) 
			{
				current = current->next;
				temp = current;
			}
			current->next = link;
			current = current->next;
			current->data = temp->data+1; // "pushing" node / adding to stacks
			}
		else
		{
			head = link; //make the new node the head
			link->data = 0; //back to null
		}
	top = link; //top of stack
	link->Title = Title; //to title
	link->checker = false; //to check
	link->Playlist = 0; //playlist
}

void Stack::Pop() //delete stacks
{
	current=head; 
	temp=head;
//	int newTopData;
	if(current->next==NULL||current==NULL) //check if empty stack
	{
		cout<<"Your Playlist is empty."<<endl;
	}
	else
	{
		while(current->data!=top->data-1) //loop for checking the num of stacks
		{
			temp=current;
			current=current->next;
		}
		if(current->data==0) //empty 
		{
			current->next=NULL;
			top=current;
			cout<<"No more music in the Playlist."<<endl;	
		}
		else //remover
		{
			top=current;
			current->next = NULL;
		}	
	}
}

void Stack::Display() //display list of songs
{
	cout << "  Music List " << endl;
	for(int i = top->data ;i>=0 ;i--)	
	{
		current=head;
		temp=head;
		while(current!=NULL &&current->data!=i) //stack is not empty
		{
			current = current->next;
		}
		if(current==NULL||current->data==0)
		{
		}
		else
		{
			cout << current->data << ") " << current->Title << endl;
		}
	} 
	cout << "" << endl;
	cout << "Number of Songs: " << top->data <<endl;
	cout << "" << endl;
}

void Stack::viewPlaylist() //display playlist
{
	for(int i = top->data;i>=0;i--)
	{
		current=head;
		temp=head;
		while(current!=NULL&&current->Playlist!=i)
		{
			current = current->next;
		}
		if(current==NULL||current->data==0||current->Playlist==0||current->checker==false)
		{
		}
		else
		{
			cout << current->Playlist << ") " << current->Title << endl;
		}
	}
}

int Stack::addMusic(int add, int count) //add to playlist
{
	pointer answer = NULL;
	current=head;
	temp=head;
	while(current != NULL && current-> data != add) 
	{
		temp = current;
		current = current->next;
	}
	if (current == NULL or current->data == 0) // if number not on the list
	{
		cout << "The music number " << add << " is not on the playlist." << endl;
		delete answer;
		return 0;
	}
	else if(current->checker==true) // delete if true
	{
		return 0;
	}
	else
	{	
		current->Playlist = count;
		current->checker = true;
		return 1; 
	}
}

int Stack::removeMusic(int count)
{
	int delMusic = count -1;
		pointer answer = NULL; 
		current=head;
		temp=head;
		while(current != NULL && current-> Playlist != delMusic)
		{
			temp = current;
			current = current->next;
		}
		if (current == NULL or current->data ==0)
		{
			delete answer;
			return 0;
		}
		else if(current->checker==false)
		{
			return 0;
		}
		else
		{	
			current->Playlist = 0;
			current->checker =false;
			return 1; 
		}
}
